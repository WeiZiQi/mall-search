package com.sunny.search.api.controller;

import com.sunny.core.constants.ResultConstant;
import com.sunny.core.model.ResultInfo;
import com.sunny.search.api.service.SellerGoodsService;
import com.sunny.search.model.entity.SellerGoodsInfo;
import com.sunny.search.model.pojo.query.SellerGoodsQuery;
import com.sunny.search.model.pojo.vo.PageAggsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 商品活动信息
 * @author fengxiangyang
 * @date 2018/12/20
 */
@RestController
@RequestMapping("b2r/search")
public class B2rSearchController {

    @Autowired
    private SellerGoodsService sellerGoodsService;

    @RequestMapping(value = "sellerGoods", method = RequestMethod.POST)
    public ResultInfo<List<SellerGoodsInfo>> sellerGoods(@RequestBody SellerGoodsQuery query) {
        ResultInfo result = new ResultInfo();
        final PageAggsVO<SellerGoodsInfo> pageAggsVO = sellerGoodsService.search(query);
        result.setData(pageAggsVO);
        result.setCode(ResultConstant.CODE_SUCCESS);
        result.setMsg("查询成功");
        return result;
    }

}
