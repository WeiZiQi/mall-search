package com.sunny.search.api.service;

import com.sunny.search.model.entity.SellerGoodsInfo;
import com.sunny.search.model.pojo.query.SellerGoodsQuery;
import com.sunny.search.model.pojo.vo.PageAggsVO;

import java.util.regex.Pattern;

/**
 * @author fengxiangyang
 * @date 2018/12/24
 */
public interface SellerGoodsService {
    /**
     * 索引名称
     */
    String INDEX_NAME = "b2r_seller_goods";
    /**
     * 判断数字正则
     */
    Pattern NUMBER_PATTERN = Pattern.compile("^[0-9]*$");
    /**
     * 召回数量
     */
    int RECALL_SIZE = 300;

    /**
     * 分页搜索信息
     * @param query
     * @return
     */
    PageAggsVO<SellerGoodsInfo> search(SellerGoodsQuery query);
}