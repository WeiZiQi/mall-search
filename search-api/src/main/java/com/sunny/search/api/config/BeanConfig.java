package com.sunny.search.api.config;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author fengxiangyang
 * @dete 2018/12/24
 */
@Configuration
public class BeanConfig {
    /**
     * HttpMessageConverters，以支持fastjson
     */
    @Bean
    public HttpMessageConverters fastJsonHttpMessageConverters() {
        return new HttpMessageConverters(new FastJsonHttpMessageConverter());
    }
}