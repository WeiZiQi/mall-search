package com.sunny.search.model.pojo.common;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * ES排序
 * @author fengxiangyang
 */
public class SortInfo {
    /**
     * 排序字段
     */
    private String field;
    /**
     * 排序方式
     */
    private Order order;
    /**
     * 排序优先级
     */
    private Integer sequence;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public enum Order{
        /**正序*/
        ASC,
        /**倒序*/
        DESC
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
