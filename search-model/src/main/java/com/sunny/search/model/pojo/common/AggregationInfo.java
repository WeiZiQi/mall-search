package com.sunny.search.model.pojo.common;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * ES聚合类
 */
public class AggregationInfo {
    /**
     * 聚合名称
     */
    private String name;
    /**
     * 聚合字段
     */
    private String field;
    /**
     * 聚合最大数量
     */
    private Integer size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
