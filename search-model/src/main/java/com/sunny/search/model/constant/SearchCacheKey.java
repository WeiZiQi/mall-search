package com.sunny.search.model.constant;

/**
 * @author fengxiangyang
 * @date 2018/12/3
 */
public enum SearchCacheKey {
    /**商品属性*/
    GOODS_ATTR,
    /**品牌*/
    BRAND,
    /**分类*/
    CATEGORY;
    private final String value;

    SearchCacheKey() {
        this.value = "SEARCH:BUSINESS:" + name();
    }

    public String key() {
        return value;
    }

    public String key(Object... params) {
        StringBuilder key = new StringBuilder(value);
        if (params != null && params.length > 0) {
            for (Object param : params) {
                key.append(':');
                key.append(String.valueOf(param));
            }
        }
        return key.toString();
    }
}
