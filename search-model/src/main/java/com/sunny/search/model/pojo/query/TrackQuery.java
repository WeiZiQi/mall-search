package com.sunny.search.model.pojo.query;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

/**
 * 调用链路
 * @author fengxiangyang
 * @date 2019/3/21
 */
public class TrackQuery implements Serializable {
    /**
     * 接口调用链路id
     */
    private String trackId;

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
