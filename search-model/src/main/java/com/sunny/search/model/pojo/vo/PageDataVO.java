package com.sunny.search.model.pojo.vo;

import com.sunny.search.model.pojo.query.PageQuery;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;

/**
 * @author fengxiangyang
 * @param <T>
 */
public class PageDataVO<T> extends PageQuery {
    /**
     * 数据总数
     */
    private Integer total;
    /**
     * 总页数
     */
    private Integer totalPages;
    private List<T> dataList;

    public PageDataVO() {
        super();
    }

    public PageDataVO(Integer page, Integer size, List<T> dataList) {
        super(page,size);
        this.total = dataList.size();
        this.totalPages = (this.total + size - 1) / size;
        this.dataList = dataList;
    }

    public PageDataVO(Integer total, Integer page, Integer size, List<T> dataList) {
        super(page,size);
        this.total = total;
        this.totalPages = (total + size - 1) / size;
        this.dataList = dataList;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }


    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public List<T> getDataList() {
        return dataList;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
