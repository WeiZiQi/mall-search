package com.sunny.search.model.pojo.vo;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 *
 * @author fengxiangyang
 * @param <T>
 */
public class PageAggsVO<T> extends PageDataVO<T> {
    private Map<String,Collection<?>> aggregations;

    public PageAggsVO() {
        super();
    }

    public PageAggsVO(Integer page, Integer size, List<T> dataList) {
        super(page,size,dataList);
    }

    public PageAggsVO(Integer total, Integer page, Integer size, List<T> dataList) {
        super(total,page,size,dataList);
    }

    public PageAggsVO(Integer total, Integer page, Integer size, List<T> dataList,Map<String,Collection<?>> aggregations) {
        super(total, page, size, dataList);
        this.aggregations = aggregations;
    }

    public Map<String, Collection<?>> getAggregations() {
        return aggregations;
    }

    public void setAggregations(Map<String, Collection<?>> aggregations) {
        this.aggregations = aggregations;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
