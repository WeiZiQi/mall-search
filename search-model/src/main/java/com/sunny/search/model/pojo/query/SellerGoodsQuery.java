package com.sunny.search.model.pojo.query;

import com.sunny.search.model.pojo.common.AggregationInfo;
import com.sunny.search.model.pojo.common.HighlightInfo;
import com.sunny.search.model.pojo.common.SortInfo;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * @author fengxiangyang
 * @date 2019/2/20
 */
public class SellerGoodsQuery extends PageQuery implements Serializable {
    /**
     * 关键字
     */
    private String key;
    /**
     * 返回字段
     */
    private String[] fields;
    /**
     * 高亮
     */
    private HighlightInfo highlightInfo;
    /**
     * 聚合
     */
    private List<AggregationInfo> aggregationInfoList;
    /**
     * 排序
     */
    private List<SortInfo> sortInfoList;
    /**
     * 分类编号
     */
    private String categoryNo;
    /**
     * 品牌id
     */
    private List<Integer> brandIds;
    /**
     * 属性值id
     */
    private List<Long> attrValueIds;
    /**
     * 查询是否改写
     */
    private boolean rewriteFlag;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String[] getFields() {
        return fields;
    }

    public void setFields(String[] fields) {
        this.fields = fields;
    }

    public HighlightInfo getHighlightInfo() {
        return highlightInfo;
    }

    public void setHighlightInfo(HighlightInfo highlightInfo) {
        this.highlightInfo = highlightInfo;
    }

    public List<AggregationInfo> getAggregationInfoList() {
        return aggregationInfoList;
    }

    public void setAggregationInfoList(List<AggregationInfo> aggregationInfoList) {
        this.aggregationInfoList = aggregationInfoList;
    }

    public List<SortInfo> getSortInfoList() {
        return sortInfoList;
    }

    public void setSortInfoList(List<SortInfo> sortInfoList) {
        this.sortInfoList = sortInfoList;
    }

    public String getCategoryNo() {
        return categoryNo;
    }

    public void setCategoryNo(String categoryNo) {
        this.categoryNo = categoryNo;
    }

    public List<Integer> getBrandIds() {
        return brandIds;
    }

    public void setBrandIds(List<Integer> brandIds) {
        this.brandIds = brandIds;
    }

    public List<Long> getAttrValueIds() {
        return attrValueIds;
    }

    public void setAttrValueIds(List<Long> attrValueIds) {
        this.attrValueIds = attrValueIds;
    }

    public boolean isRewriteFlag() {
        return rewriteFlag;
    }

    public void setRewriteFlag(boolean rewriteFlag) {
        this.rewriteFlag = rewriteFlag;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
