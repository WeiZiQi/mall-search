package com.sunny.search.model.pojo.query;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

/**
 * @author fengxiangyang
 * @date 2018/12/24
 */
public class PageQuery extends TrackQuery implements Serializable {
    /**
     * 默认开始页
     */
    public static final int DEFAULT_PAGE = 0;
    /**
     * 默认每页数量
     */
    public static final int DEFAULT_SIZE = 10;
    /**
     * 第几页
     */
    private int pageNo;
    /**
     * 每页的数量
     */
    private int pageSize;

    public PageQuery() {
        this.pageNo = DEFAULT_PAGE;
        this.pageSize = DEFAULT_SIZE;
    }

    public PageQuery(int pageNo, int pageSize) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }

    public static int getDefaultPage() {
        return DEFAULT_PAGE;
    }

    public static int getDefaultSize() {
        return DEFAULT_SIZE;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
