package com.sunny.search.model.pojo.common;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;

/**
 * ES高亮
 * @author fengxiangyang
 */
public class HighlightInfo {
    /**
     * 前标签
     */
    private String preTag;
    /**
     * 后标签
     */
    private String postTag;
    /**
     * 需高亮字段
     */
    private List<String> fields;

    public String getPreTag() {
        return preTag;
    }

    public void setPreTag(String preTag) {
        this.preTag = preTag;
    }

    public String getPostTag() {
        return postTag;
    }

    public void setPostTag(String postTag) {
        this.postTag = postTag;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
